<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>My shit vhost wizard</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.2.0/yeti/bootstrap.min.css" media="screen">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">VirtualHost Wizard</a>
            </div>
            <div class="navbar-collapse collapse">
                <?php /*<ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link</a></li>
                </ul>*/ ?>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Host Information</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            if(!$_POST) {
                                header("Location: index.php");
                            }
                            if($_POST) {
                                $failureURL = "index.php?u=".$_POST['ServerAlias']."&e=".$_POST['ServerAdmin'];

                                $error = false;
                                $errorMessage = "";
                                $website = addslashes($_POST['ServerAlias']);
                                $serverAdmin = addslashes($_POST['ServerAdmin']);

                                if(!isURL($website)) {
                                    $error = true;
                                    $errorMessage .= "
                                    <div class='alert alert-dismissable alert-danger'>
                                        <button type='button' class='close' data-dismiss='alert'>×</button>
                                        <strong>Nope!</strong> This website <a href='{$website}''>{$website}</a> doesn't look valid.
                                    </div>";
                                }

                                if(file_exists("/var/wwww/{$website}")) {
                                    $error = true;
                                    $errorMessage .= "
                                    <div class='alert alert-dismissable alert-danger'>
                                        <button type='button' class='close' data-dismiss='alert'>×</button>
                                        <strong>Nope!</strong> This website <a href='{$website}''>{$website}</a> already exists on this server.
                                    </div>";
                                }

                                if(!filter_var($serverAdmin, FILTER_VALIDATE_EMAIL)) {
                                    $error = true;
                                    $errorMessage .= "
                                    <div class='alert alert-dismissable alert-danger'>
                                        <button type='button' class='close' data-dismiss='alert'>×</button>
                                        <strong>Nope!</strong> This email address <i>{$serverAdmin}</i> doesn't look valid. You need a valid email address.
                                    </div>";
                                }

                                if($error) {
                                    echo '
                                    <div class="jumbotron">
                                        <div class="container">
                                            <h1>Error!</h1>
                                            '. $errorMessage .'
                                            <p><a href="'.failureURL.'" class="btn btn-primary btn-lg" role="button">Fix Errors »</a></p>
                                        </div>
                                    </div>';

                                }
                                else {

                                    $bootswatch = array("cerulean","cosmo","cyborg","darkly","flatly","journal","lumen","paper","readable","sandstone","simplex","slate","spacelab","superhero","united","yeti");
                                    $randkey = mt_rand(0, count($bootswatch));
                                    $randtheme = $bootswatch[$randkey];

$vhost = <<<PHP
<VirtualHost *:80>
    ServerName {$website}
    UseCanonicalName on
    ServerAlias {$website}
    ServerAdmin {$serverAdmin}
    DocumentRoot /var/www/{$website}/web
    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
PHP;

$index = <<<PHP
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome!</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.2.0/{$randtheme}/bootstrap.min.css" media="screen">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">{$website}</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->

        <div class="container">
            <div>
                <h1>Welcome!</h1>
                <p class="lead">Welcome to your new website. The file you see now is <pre>/var/www/{$website}/web/index.php</pre>.</p>
            </div>
        </div>
    </body>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</html>
PHP;
                                    
                                    system('sudo mkdir -p /var/wwww/{$website}/web');
                                    system('sudo chmod -R 755 /var/www/{$website}');
                                    file_put_contents("{$website}.conf", $vhost);
                                    system('sudo cp {$website}.conf /etc/apache2/sites-available/{$website}.conf');
                                    //system("rm {$website}.conf");
                                    file_put_contents("{$website}.php", $index);
                                    system('sudo cp {$website}.php /var/www/{$website}/index.php');
                                    //system("rm {$website}.php");
                                    
                                    

                                    echo "
                                    <h1>Congratulations</h1>
                                    <p>Your website <a href='http://{$website}'>{$website}</a> is now active.</p>
                                    ";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</html>

<?php
function TD() {
    return round( ( microtime(true) - $_SERVER['REQUEST_TIME'] ) , 3 ) .'s.';
}

function isURL($url) {
    $regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
    $regex .= "(\:[0-9]{2,5})?"; // Port 
    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 

    if(preg_match("/^$regex$/", $url)) { 
        return true; 
    }
    else {
        return false;
    }
}
?>
