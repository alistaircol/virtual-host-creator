<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>My shit vhost wizard</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.2.0/yeti/bootstrap.min.css" media="screen">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">VirtualHost Wizard</a>
            </div>
            <div class="navbar-collapse collapse">
                <?php /*<ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link</a></li>
                </ul>*/ ?>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Host Information</h3>
                        </div>
                        <div class="panel-body">
                            <form action="app.php" id="hostForm" method="POST">
                                <label class="control-label">Website URL</label>
                                <input type="text" class="form-control" name="ServerAlias" placeholder="website.com">

                                <label class="control-label">Admin Email</label>
                                <input type="text" class="form-control" name="ServerAdmin" placeholder="admin@website.com">
                                
                                <br /><br />
                                <input type="submit" value="Setup" class="btn btn-lg btn-primary center-block" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
</html>
