# Virtual Host Creator

For Apache 2 (tested on Ubuntu).

## Bash script

A decent bash script to create virtual host and file structure (optionally git clone).

```
$ sudo bash makehost.sh http://subdomain.domain.com
$ sudo bash makehost.sh http://subdomain.domain.com https://bitbucket.org/alistaircol/virtual-host-creator.git
```

## PHP script

A real shitty virtual host creator in.