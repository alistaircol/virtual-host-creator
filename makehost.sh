#!/bin/bash
R="\x1b[31m"
G="\x1b[32m"
Y="\x1b[33m"
B="\x1b[34m"
W="\x1b[37m"
CLR="\x1b[0m"

if [ "$#" -eq 1 ]; then
    echo -e $B"Creating virtual host and structure for: $1."$CLR
    cd
    mkdir -p /var/www/$1/web
    echo -e "mkdir -p /var/www/$1/web"$G"                      Done." $CLR
    chmod -R 755 /var/www/$1/web
    echo -e "chmod -R 755 /var/www/$1/web"$G"                  Done." $CLR
    echo "<VirtualHost *:80>
    ServerAdmin admin@$1
    ServerName $1
    ServerAlias $1
    DocumentRoot /var/www/$1/web
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" >> /etc/apache2/sites-available/$1.conf
    echo -e "vhost >> /etc/apache2/sites-available/$1.conf"$G" Done." $CLR
    a2ensite $1
    echo -e "a2ensite $1"$G"                                   Done." $CLR
    service apache2 reload
    echo -e "service apache2 reload"$G"                        Done." $CLR
    echo -e $B"Done!"$CLR
    exit 1
elif [ "$#" -eq 2 ]; then
    echo -e $B"Creating virtual host and structure for: $1 with git clone." $CLR
    cd
    mkdir -p /var/www/$1/web
    echo -e "mkdir -p /var/www/$1/web"$G"                      Done." $CLR
    chmod -R 755 /var/www/$1/web
    echo -e "chmod -R 755 /var/www/$1/web"$G"                  Done." $CLR
    echo "<VirtualHost *:80>
    ServerAdmin admin@$1
    ServerName $1
    ServerAlias $1
    DocumentRoot /var/www/$1/web
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" >> /etc/apache2/sites-available/$1.conf
    echo -e "vhost >> /etc/apache2/sites-available/$1.conf"$G" Done." $CLR
    a2ensite $1
    echo -e "a2ensite $1"$G"                                   Done." $CLR
    service apache2 reload
    echo -e "service apache2 reload"$G"                        Done." $CLR
    git clone $2 /var/www/$1/web
    echo -e "git clone $2 /var/www/$1/web"$G"                  Done." $CLR
    echo -e $B"Done!"$CLR
    exit 1
else
    echo "USAGE: bash mkvhost.sh <domain> [<git repo URL>]"
    exit 1
fi
